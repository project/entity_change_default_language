<?php

namespace Drupal\entity_change_default_language;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Utility\Error;
use Psr\Log\LoggerInterface;

/**
 * Helper service for changing the default langcode of an entity.
 */
class EntityChangeDefaultLanguage implements EntityChangeDefaultLanguageInterface {

  /**
   * Constructs a new EntityChangeDefaultLanguage object.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The logger channel factory service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(protected LoggerChannelFactoryInterface $loggerFactory, protected EntityTypeManagerInterface $entityTypeManager) {}

  /**
   * {@inheritdoc}
   */
  public function update(ContentEntityInterface $entity, string $default_langcode, bool $create = FALSE, array $langcodes = []): bool {
    try {
      static $translated_entities;

      // Do not translate the same entity more than one time.
      if (isset($translated_entities[$entity->getEntityTypeId()][$entity->id()])) {
        return TRUE;
      }
      $translated_entities[$entity->getEntityTypeId()][$entity->id()] = TRUE;

      // Early return if the entity is not translatable.
      if (!$entity->isTranslatable()) {
        return FALSE;
      }

      // Ensure we have the original language.
      $entity = $entity->getUntranslated();
      $original_langcode = $entity->language()->getId();

      // Early return if the default langcode is the same.
      if ($original_langcode == $default_langcode) {
        return TRUE;
      }

      // Early return if the entity does not have the new default language,
      // and we do not have to create it.
      if (!$create && !$entity->hasTranslation($default_langcode)) {
        return FALSE;
      }

      // Get values for later use.
      $original_values = $entity->toArray();
      $original_values['content_translation_source'] = $default_langcode;
      if ($entity->hasTranslation($default_langcode)) {
        $translated_entity = $entity->getTranslation($default_langcode);
        $new_values = $translated_entity->toArray();
      }
      // If the entity does not have the translation, we create one from the
      // default language.
      else {
        $translated_entity = $entity;
        $new_values = $original_values;
        $new_values['langcode'] = $default_langcode;
      }
      $new_values['content_translation_source'] = LanguageInterface::LANGCODE_NOT_SPECIFIED;

      // Update referenced entities recursively.
      $field_definitions = $translated_entity->getFieldDefinitions();
      foreach ($field_definitions as $field_name => $field_definition) {
        if (in_array($field_definition->getType(), ['entity_reference', 'entity_reference_revisions'])) {
          foreach ($translated_entity->get($field_name)->referencedEntities() as $referenced_entity) {
            if ($referenced_entity instanceof ContentEntityInterface) {
              $this->update($referenced_entity, $default_langcode, $create, $langcodes);
            }
          }
        }
      }

      // The removed translation needs to be saved, or we can not change the
      // language to that.
      if ($entity->hasTranslation($default_langcode)) {
        $entity->removeTranslation($default_langcode);
        $this->entitySave($entity);
      }

      // Set the new values.
      $this->updateValues($entity, $new_values);

      // Restore the original values if we want to keep this translation.
      if (in_array($original_langcode, $langcodes)) {
        $translation = $entity->addTranslation($original_langcode);
        $this->updateValues($translation, $original_values);
      }

      // Cleanup translations.
      foreach ($entity->getTranslationLanguages(FALSE) as $langcode => $language) {
        if (!in_array($langcode, $langcodes)) {
          $entity->removeTranslation($langcode);
        }
        else {
          $translation = $entity->getTranslation($langcode);
          $translation->set('content_translation_source', $default_langcode);
        }
      }
      $this->entitySave($entity);
    }
    catch (\Exception $exception) {
      if (method_exists(Error::class, 'logException')) {
        Error::logException($this->getLogger(), $exception);
      }
      else {
        watchdog_exception('entity_change_default_language', $exception);
      }
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Updates the entity values.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to save.
   * @param array $values
   *   The values to update.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function updateValues(ContentEntityInterface $entity, array $values): void {
    // Ensure we don't change the default langcode.
    unset($values['default_langcode']);

    // Set only the translatable fields.
    foreach ($values as $field_name => $field_values) {
      if ($entity->getFieldDefinition($field_name)->isTranslatable()) {
        $entity->set($field_name, $field_values);
      }
    }
  }

  /**
   * Saves the entity without generate a new revision.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to save.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   In case of failures, an exception is thrown.
   */
  protected function entitySave(ContentEntityInterface $entity): void {
    if ($entity->isNewRevision()) {
      $entity->setNewRevision(FALSE);
    }
    $entity->setSyncing(TRUE);
    $entity->save();
  }

  /**
   * Gets the logger for a specific channel.
   *
   * @param string $channel
   *   The name of the channel. Defaults "entity_change_default_language".
   *
   * @return \Psr\Log\LoggerInterface
   *   The logger for the given channel.
   */
  protected function getLogger(string $channel = 'entity_change_default_language'): LoggerInterface {
    return $this->loggerFactory->get($channel);
  }

}
