<?php

namespace Drupal\entity_change_default_language\Drush\Commands;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\entity_change_default_language\EntityChangeDefaultLanguageInterface;
use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A Drush command file.
 */
class EntityChangeDefaultLanguageCommands extends DrushCommands {

  /**
   * Constructs an EntityChangeDefaultLanguageCommands object.
   *
   * @param \Drupal\entity_change_default_language\EntityChangeDefaultLanguageInterface $entityChangeDefaultLanguage
   *   The entity change default language service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Queue\QueueFactory $queue
   *   The queue factory.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   */
  public function __construct(protected EntityChangeDefaultLanguageInterface $entityChangeDefaultLanguage, protected EntityTypeManagerInterface $entityTypeManager, protected QueueFactory $queue, protected LanguageManagerInterface $languageManager) {
    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_change_default_language'),
      $container->get('entity_type.manager'),
      $container->get('queue'),
      $container->get('language_manager'),
    );
  }

  /**
   * Changes the language of an entity.
   */
  #[CLI\Command(name: 'entity_change_default_language:change-default-language', aliases: ['ecdl:cdl'])]
  #[CLI\Argument(name: 'entity_type_id', description: 'Entity type.')]
  #[CLI\Argument(name: 'entity_id', description: 'Entity id.')]
  #[CLI\Argument(name: 'default_langcode', description: 'New default language.')]
  #[CLI\Option(name: 'preserve-legacy-default-language', description: 'Preserves the default legacy default language as a translation.')]
  #[CLI\Option(name: 'preserve-languages', description: 'List of translation languages to preserve.')]
  #[CLI\Usage(name: 'entity_change_default_language:change-default-language node 1 en --preserve-languages=en', description: 'Change the default language for node 1 into english.')]
  public function changeDefaultLanguage(string $entity_type_id, int $entity_id, string $default_langcode, array $options = []): void {
    $options += [
      'preserve-legacy-default-language' => TRUE,
      'preserve-languages' => [],
    ];
    $entity = $this->getSafeEntity($entity_type_id, $entity_id);
    $language = $this->getSafeLanguage($default_langcode);

    if ($entity->getUntranslated()->language()->getId() == $language->getId()) {
      throw new \Exception(dt('Entity is already created in ":language" language', [':language' => $language->getName()]));
    }

    if ($this->io()->confirm(dt('Are you want to change the :entity_type :entity_label default language to :default_langcode?', [
      ':entity_type' => $entity->getEntityTypeId(),
      ':entity_label' => $entity->label(),
      ':default_langcode' => $language->getName(),
    ]), FALSE)) {
      $this->updateEntity($entity, $language, $options);
    }
  }

  /**
   * Change the default langcode for an entity type.
   *
   * @param string $entity_type_id
   *   Entity type ID.
   * @param string $default_langcode
   *   New default language.
   * @param array $options
   *   Options to change the default language.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  #[CLI\Command(name: 'entity_change_default_language:change-default-language-entity-type', aliases: ['ecdl:dlet'])]
  #[CLI\Argument(name: 'entity_type_id', description: 'Entity type.')]
  #[CLI\Argument(name: 'default_langcode', description: 'New default language.')]
  #[CLI\Option(name: 'bundle', description: 'Entity type bundle.')]
  #[CLI\Option(name: 'preserve-legacy-default-language', description: 'Preserves the default legacy default language as a translation.')]
  #[CLI\Option(name: 'preserve-languages', description: 'List of translation languages to preserve.')]
  #[CLI\Usage(name: 'entity_change_default_language:change-default-language node en --preserve-languages=en', description: 'Change the default language for node 1 into english.')]
  public function changeDefaultLanguageForEntityType(string $entity_type_id, string $default_langcode, array $options = []): void {
    $options += [
      'bundle' => NULL,
      'preserve-legacy-default-language' => TRUE,
      'preserve-languages' => [],
    ];
    $entity_type = $this->getSafeEntityType($entity_type_id);
    $language = $this->getSafeLanguage($default_langcode);
    $query = $this->entityTypeManager->getStorage($entity_type_id)->getQuery();
    $query->accessCheck(FALSE);
    if (!empty($options['bundle'])) {
      $query->condition('bundle', $options['bundle']);
    }
    $query->condition('langcode', $language->getId(), '!=');

    $entities = $query->execute();
    $this->logger()->info(sprintf('%s entities found.', count($entities)));
    if ($this->io()->confirm(dt('Are you sure you want to change of these entities languages to :default_langcode?', [
      ':default_langcode' => $language->getName(),
    ]), FALSE)) {
      foreach ($entities as $entity_id) {
        $queue_item = [
          'entity_type' => $entity_type->id(),
          'default_langcode' => $default_langcode,
          'entity_id' => $entity_id,
          'preserve_legacy_default_language' => $options['preserve-legacy-default-language'],
          'preserve_languages' => $options['preserve-languages'],
        ];

        $this->queue->get('entity_change_default_language')->createItem($queue_item);
      }

      $this->logger()->success('Entities enqueued');
    }
  }

  /**
   * Get the entity type checking it is a correct one.
   *
   * If entity type does not exist, or it is not translatable,
   * it will throw an exception.
   *
   * @param string $entity_type_id
   *   Entity type ID.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface
   *   Entity type.
   *
   * @throws \Exception
   */
  protected function getSafeEntityType(string $entity_type_id): EntityTypeInterface {
    $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
    if (!$entity_type instanceof EntityTypeInterface) {
      throw new \Exception(sprintf('%s entity type not found!', $entity_type_id));
    }

    if (!$entity_type->isTranslatable()) {
      throw new \Exception(sprintf('%s entity type not translatable!', $entity_type->getLabel()));
    }

    return $entity_type;
  }

  /**
   * Get the entity checking it is a correct one.
   *
   * If entity  not exist, or it is not translatable,
   * it will throw an exception.
   *
   * @param string $entity_type_id
   *   Entity type id.
   * @param int $entity_id
   *   Entity ID.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   Entity.
   *
   * @throws \Exception
   */
  protected function getSafeEntity(string $entity_type_id, int $entity_id): ContentEntityInterface {
    $entity_type = $this->getSafeEntityType($entity_type_id);

    $entity = $this->entityTypeManager->getStorage($entity_type->id())->load($entity_id);
    if (!$entity instanceof ContentEntityInterface) {
      throw new \Exception(sprintf('%s entity with id %s not found!', $entity_type->getLabel(), $entity_id));
    }

    if (!$entity->isTranslatable()) {
      throw new \Exception(sprintf('%s entity  "%s" is not translatable!', $entity_type->getLabel(), $entity->label()));
    }

    return $entity;
  }

  /**
   * Get the language checking it exists.
   *
   * If it does not exist,
   *
   * @param string $default_langcode
   *   Default language code.
   *
   * @throws \Exception
   */
  protected function getSafeLanguage(string $default_langcode) {
    $language = $this->languageManager->getLanguage($default_langcode);
    if (!$language instanceof LanguageInterface) {
      throw new \Exception(sprintf('Language %s not found!', $default_langcode));
    }

    return $language;
  }

  /**
   * Updates the entity default langcode.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity which language will change.
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   Language the entity will be changed to.
   * @param array $options
   *   Options to change the default language.
   */
  protected function updateEntity(ContentEntityInterface $entity, LanguageInterface $language, array $options): void {
    if ($this->entityChangeDefaultLanguage->update($entity, $language->getId(), $options['preserve-legacy-default-language'], $options['preserve-languages'])) {
      $this->logger()->success('Entity default language changed successfully!');
    }
    else {
      $this->logger()->error('There was a problem updating entity default language, check logs for details');
    }
  }

}
