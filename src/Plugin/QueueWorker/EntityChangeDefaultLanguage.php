<?php

declare(strict_types=1);

namespace Drupal\entity_change_default_language\Plugin\QueueWorker;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\entity_change_default_language\EntityChangeDefaultLanguageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Asynchronously changes the default langcode of an entity.
 *
 * @QueueWorker(
 *   id = "entity_change_default_language",
 *   title = @Translation("Change default langcode"),
 *   cron = {"time" = 60},
 * )
 */
class EntityChangeDefaultLanguage extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs a new EntityChangeDefaultLanguage instance.
   *
   * @param array $configuration
   *   Configuration.
   * @param string $plugin_id
   *   Plugin ID.
   * @param mixed $plugin_definition
   *   Plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\entity_change_default_language\EntityChangeDefaultLanguageInterface $entityChangeDefaultLanguage
   *   The entity change default language service.
   */
  public function __construct(array $configuration, string $plugin_id, mixed $plugin_definition, protected EntityTypeManagerInterface $entityTypeManager, protected EntityChangeDefaultLanguageInterface $entityChangeDefaultLanguage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_change_default_language'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    $entity = $this->getEntityFromQueueData($data);
    // Only do the operation with entities translated to target language.
    if ($entity instanceof ContentEntityInterface) {
      $has_translation = $entity->hasTranslation($data['default_langcode']);
      $is_not_default = $entity->getUntranslated()->language()->getId() != $data['default_langcode'];

      if ($has_translation && $is_not_default) {
        $this->entityChangeDefaultLanguage->update($entity, $data['default_langcode'], $data['preserve_legacy_default_language'], $data['preserve_languages']);
      }
    }
  }

  /**
   * Get the entity from the queue data.
   *
   * @param array $data
   *   Data.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|null
   *   The entity or NULL if not found.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getEntityFromQueueData(array $data): ?ContentEntityInterface {
    $entity_type = $this->entityTypeManager->getDefinition($data['entity_type']);
    if ($entity_type instanceof EntityTypeInterface) {
      $entity = $this->entityTypeManager->getStorage($data['entity_type'])->load($data['entity_id']);

      return $entity instanceof ContentEntityInterface ? $entity : NULL;
    }

    return NULL;
  }

}
