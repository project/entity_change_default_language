<?php

namespace Drupal\entity_change_default_language;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Defines an interface for the Entity Change Default Language service.
 */
interface EntityChangeDefaultLanguageInterface {

  /**
   * Updates the default language of an entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to be updated.
   * @param string $default_langcode
   *   The new default langcode.
   * @param bool $create
   *   (optional) Whether to create or not the new default translation if
   *   it not exists, using the current default translation as source.
   * @param array $langcodes
   *   The list of lang codes to preserve, empty array to delete all
   *   existing translations.
   *
   * @return bool
   *   TRUE if the entity has been updated, otherwise FALSE.
   */
  public function update(ContentEntityInterface $entity, string $default_langcode, bool $create = FALSE, array $langcodes = []): bool;

}
